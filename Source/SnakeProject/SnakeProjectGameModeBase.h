// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEPROJECT_API ASnakeProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
